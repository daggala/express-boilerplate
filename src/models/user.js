
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {

    id:{
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV1,
      primaryKey: true,
    },
    username: DataTypes.TEXT,
  },

  {
    classMethods: {
      associate: function(models) {
        User.hasMany(models.Missions);
      },
    },
  }
  );

  return User;
};
