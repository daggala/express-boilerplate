import express from 'express';
import missionRoutes from './mission';
import userRoutes from './user';

const apiRoutes = express.Router();

apiRoutes.use('/mission', missionRoutes);
apiRoutes.use('/user', userRoutes);


module.exports = apiRoutes;
