import express from 'express';
import winston from 'winston';
import _ from 'lodash';

import { User } from '../models';
const apiRoutes = express.Router();

function createUser(res, newUser){
  return User.create(newUser)
    .then((result) => {
      winston.info(`Create a user with id ${result.id}`);
      return res.status(200).send({ success : true, msg : 'User created', result : result });
    })
    .catch(err => {
      winston.error(`Error on POST /user : ${err}`);
      winston.debug(`Error on POST /user : ${err.stack}`);
      return res.status(400).send({success : false, msg : 'User creation failed'});
    });
}

function updUser(res, editUser){
  return User.update(editUser, {
    where: {id: editUser.id},
    returning: true,
  })
    .then((result) => {

      //the update function returns an array
      let user = {};
      user = result[1][0];
      winston.info(`Update a user with id ${user.id}`);
      return res.status(200).send({ success : true, msg : 'User updated', user : user });
    })
    .catch(err => {
      winston.error(`Error on POST /user/${editUser.id} : ${err}`);
      winston.debug(`Error on POST /user/${editUser.id} : ${err.stack}`);
      return res.status(400).send({success : false, msg : 'User update failed'});
    });
}

/**
* @api {get} /user/:id Get user
* @apiVersion 1.0.0
* @apiName GetUser
* @apiGroup User
* @apiDescription This API Method is used to get a user
*
* @apiParam {STRING} id User id
*
* @apiSuccess {JSON} user Detail informations about the User.
*/
apiRoutes
  .get('/:userId', (req, res) => {
    const userId = req.params.userId;
    if (!userId) {
      return res.status(400).send({success : false, msg : 'User id needed'});
    }
    User.findById(userId)
      .then(user => res.status(200).send({success : true, msg : 'User fetched', user}))
      .catch(err => {
        winston.error(`Error on GET /user/${userId} : ${err}`);
        winston.debug(`Error on GET /user/${userId}  : ${err.stack}`);
        res.status(500).send({success : false, msg : 'User not fetched'});
      });
  });

/**
* @api {post} /user User creation
* @apiVersion 1.0.0
* @apiName PostUser
* @apiGroup User
* @apiDescription This API Method is used to create a user
*
* @apiParam {STRING} username Username of the User
*
* @apiSuccess {JSON} user Detail informations about the User.
*/
apiRoutes
  .post('/', (req, res) => {
    const newUser = _.omit(_.pick(req.body, _.keys(User.attributes)), ['updatedAt', 'createdAt']);
    return createUser(res, newUser);

});

/**
* @api {put} /user/:userId User update
* @apiVersion 1.0.0
* @apiName PutUser
* @apiGroup User
* @apiDescription This API Method is used to update a user
*
* @apiParam {STRING} username Name of the User
*
* @apiSuccess {JSON} user Detail informations about the User.
*/
apiRoutes
  .put('/:userId', (req, res) => {

    const userId = req.params.userId;
    if (!userId) {
      return res.status(400).send({success : false, msg : 'User id needed'});
    }

    const updateUser = _.omit(_.pick(req.body, _.keys(User.attributes)), ['updatedAt', 'createdAt']);
    //prevent a random ID to be generated
    updateUser.id = userId;

    User.findById(userId)
      .then(user => {
        if(!user){
          winston.info(`User ${userId} does not exist - will be created`);
          return createUser(res, updateUser);
        }
        else{
          winston.info(`User ${userId} already exist - will be updated`);
          return updUser(res, updateUser);
        }
      })
      .catch(err => {
        winston.error(`Error on GET /user/${userId} : ${err}`);
        winston.debug(`Error on GET /user/${userId}  : ${err.stack}`);
        return res.status(500).send({success : false, msg : 'User not fetched'});
      });
  });


export default apiRoutes;
