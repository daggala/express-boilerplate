import express from 'express';
import winston from 'winston';
import _ from 'lodash';

import { Mission } from '../models';

const apiRoutes = express.Router();


function createMission(res, newMission){
  return Mission.create(newMission)
    .then((result) => {
      winston.info(`Create a mission with id ${result.id}`);
      return res.status(200).send({ success : true, msg : 'Mission created', result : result });
    })
    .catch(err => {
      winston.error(`Error on POST /mission/ : ${err}`);
      winston.debug(`Error on POST /mission/ : ${err.stack}`);
      return res.status(400).send({success : false, msg : 'Mission creation failed'});
    });
}

function updMission(res, editMission){

  return Mission.update(editMission, {
    where: {id: editMission.id},
    returning: true,
  })
    .then((result) => {
      //the update function returns an array
      let mission = {};
      mission = result[1][0] ;
      winston.info(`Update a mission with id ${mission.id}`);
      return res.status(200).send({ success : true, msg : 'Mission updated', mission : mission });
    })
    .catch(err => {
      winston.error(`Error on POST /mission/${editMission.id} : ${err}`);
      winston.debug(`Error on POST /mission/${editMission.id} : ${err.stack}`);
      return res.status(400).send({success : false, msg : 'Mission update failed'});
    });
}

/**
* @api {get} /mission/:id Get mission
* @apiVersion 1.0.0
* @apiName GetMission
* @apiGroup Mission
* @apiDescription This API Method is used to get a mission
*
* @apiParam {STRING} id Activity id
*
* @apiSuccess {JSON} mission Detail informations about the Mission.
*/
apiRoutes
  .get('/:id', (req, res) => {

    const missionId = req.params.id;
    if (!missionId) {
      return res.status(400).send({success : false, msg : 'Mission id needed'});
    }

    Mission.findById(missionId)
      .then(mission => res.status(200).send({success : true, msg : 'Mission fetched', mission}))
      .catch(err => {
        winston.error(`Error on GET /mission/${missionId} : ${err}`);
        winston.debug(`Error on GET /mission/${missionId}  : ${err.stack}`);
        return res.status(500).send({success : false, msg : 'Mission not fetched'});
      });
  });

/**
* @api {post} /mission/:user_id Mission creation
* @apiVersion 1.0.0
* @apiName PostMission
* @apiGroup Mission
* @apiDescription This API Method is used to create a mission
*
* @apiParam {STRING} name Name of the Activity
* @apiParam {STRING} description description of the mission,
* @apiParam {STRING} objective objective of the mission,
* @apiParam {DECIMAL} reward reward for the mission,
* @apiParam {STRING='development', 'design', 'marketing', 'testing'} type Category of the mission.
* @apiParam {STRING} imageUrl url.
*
* @apiSuccess {JSON} mission Detail informations about the Activity.
*/


apiRoutes
  .post('/', (req, res) => {
     //only pick attributes of Mission model
     const newMission  = _.omit(_.pick(req.body, _.keys(Mission.attributes)), ['updatedAt', 'createdAt']);
     if (!newMission.name || !newMission.description || !newMission.reward || !newMission.UserId) {
             winston.info('Attempt to create mission witout required fields !');
             winston.debug(`Request body ${JSON.stringify(req.body)}`);
             return res.status(403).send({success: false, msg: 'Required field are missing !'});
          }
          //set default status if omited
          newMission.status = newMission.status || 'draft';

          //if the ID is in the req body then we avoid randomly generating a new one
          if(newMission.id){
            Mission.findById(newMission.id)
              .then(mission => {
                if(!mission){
                  return createMission(res, newMission);
                }
                else{
                  winston.info(`Mission ${newMission.id} already exist`);
                  return res.status(400).send({success : false, msg : 'Mission id already exists', mission});
                }
              })
              .catch(err => {
                winston.error(`Error on GET /mission/${newMission.id} : ${err}`);
                winston.debug(`Error on GET /mission/${newMission.id}  : ${err.stack}`);
                return res.status(500).send({success : false, msg : 'Mission not fetched'});
              });
          }
          else{
            return createMission(res, newMission);
          }

  });


/**
* @api {put} /mission/:id Mission creation
* @apiVersion 1.0.0
* @apiName PutMission
* @apiGroup Mission
* @apiDescription This API Method is used to update a mission
*
* @apiParam {STRING} name Name of the Activity
* @apiParam {STRING} description description of the mission,
* @apiParam {STRING} objective objective of the mission,
* @apiParam {DECIMAL} reward reward for the mission,
* @apiParam {STRING='development', 'design', 'marketing', 'testing'} type Category of the mission.
* @apiParam {STRING} imageUrl url.
*
* @apiSuccess {JSON} mission Detail informations about the Activity.
*/

apiRoutes
  .put('/:id', (req, res) => {

    const missionId = req.params.id;
    if (!missionId) {
      return res.status(400).send({success : false, msg : 'Mission id needed'});
    }

    const updateMission  = _.omit(_.pick(req.body, _.keys(Mission.attributes)), ['updatedAt', 'createdAt']);
    //prevent a random ID to be generated
    updateMission.id = missionId;

    Mission.findById(missionId)
      .then(mission => {
        if(!mission){
          winston.info(`Mission ${missionId} does not exist - will be created`);

          if (!updateMission.name || !updateMission.description || !updateMission.reward || !updateMission.UserId) {
             winston.info('Attempt to create mission witout required fields !');
             winston.debug(`Request body ${JSON.Stringify(req.body)}`);
             return res.status(403).send({success: false, msg: 'Required field are missing !'});
          }
          //set default status if omited
          updateMission.status = updateMission.status || 'draft';

          return createMission(res, updateMission, missionId);

        }
        else{
          winston.info(`Mission ${missionId} already exists - will be updated`);
          return updMission(res, updateMission);
        }
      })
      .catch(err => {
        winston.error(`Error on GET /mission/${missionId} : ${err}`);
        winston.debug(`Error on GET /mission/${missionId}  : ${err.stack}`);
        return res.status(500).send({success : false, msg : 'Mission not fetched'});
      });
  });

export default apiRoutes;
